module.exports = {
  addons: [
    "@storybook/preset-typescript",
    "@storybook/addon-knobs",
    "@storybook/addon-storysource",
    "@storybook/addon-actions/register",
    "@storybook/addon-viewport/register",
    "@storybook/addon-backgrounds/register",
    "@storybook/addon-a11y/register",
  ],
};
