// ---------------------------------------------------------------------

import React from "react";
import Head from "next/head";
import { InferGetStaticPropsType, NextPage, GetStaticProps } from "next";

// ---------------------------------------------------------------------

import { LayoutProvider } from "../layout/";
import { HomeProvider } from "../components/v1";

// ---------------------------------------------------------------------

interface PassingProps {}

type AppProps = NextPage &
  PassingProps &
  InferGetStaticPropsType<typeof getStaticProps>;

// ---------------------------------------------------------------------

const IndexPage: React.FunctionComponent<AppProps> = () => {
  return (
    <>
      <Head>
        <head>
          <meta name="text" content="nane" />
          <meta name="description" content="description" />
          <meta name="keywords" content="keywords" />
          <title>Penbeh - Mazdak</title>
        </head>
      </Head>
      <LayoutProvider>
        <HomeProvider />
      </LayoutProvider>
    </>
  );
};

// ---------------------------------------------------------------------

export const getStaticProps: GetStaticProps = async () => {
  return { props: {} };
};

// ---------------------------------------------------------------------

export default IndexPage;

// ---------------------------------------------------------------------
