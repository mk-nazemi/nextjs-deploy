// import * from 'next/babel'
import { AppProps } from "next/app";

// The Component prop is the active page,
// pageProps is an object with the initial props that were preloaded
// for your page by one of our data fetching methods,

export default function MyApp({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />;
}
