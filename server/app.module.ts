import {
  Module,
  NestModule,
  MiddlewareConsumer,
  RequestMethod,
} from "@nestjs/common";
import { NextModule, NextMiddleware } from "@nestpress/next";
import { ConfigModule } from "@nestjs/config";

// -------------------------------------------------

import { AuthModule } from "./api/v1/auth/auth.module";
import configuration from "../config/configuration";
import { AppController } from "./app.controller";

// -------------------------------------------------

@Module({
  imports: [
    NextModule,
    ConfigModule.forRoot({ load: [configuration], isGlobal: true }),
    AuthModule,
  ],
  controllers: [AppController],
})

// -------------------------------------------------
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(NextMiddleware).forRoutes({
      path: "_next*",
      method: RequestMethod.GET,
    });

    consumer.apply(NextMiddleware).forRoutes({
      path: "*",
      method: RequestMethod.GET,
    });
  }
}
