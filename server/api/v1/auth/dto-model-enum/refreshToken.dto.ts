import { IsOptional, IsString } from 'class-validator';

export class RefreshTokenDto {
  @IsString()
  Token: string;

  @IsString()
  CurrentSalonId: string;

  @IsString()
  CurrentClientKey: string;
}
