export enum ApplicationTypeEnum {
  IOS = 1,

  Android = 2,

  Web = 3,

  CMSAdmin = 4,
}
