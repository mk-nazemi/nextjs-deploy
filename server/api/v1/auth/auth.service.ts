import axios from "axios";
import { Injectable, Logger } from "@nestjs/common";
import { ConfigService } from "@nestjs/config/dist/config.service";
import { BadRequestException } from "@nestjs/common/exceptions/bad-request.exception";
// ------------------------------ --- -----------

import {
  AuthResponseDto,
  AuthCredentialsDto,
  RefreshTokenDto,
  AuthPhoneDto,
} from "./dto-model-enum/";
// ------------------------------ --- -----------

@Injectable()
export class AuthService {
  // ------------------------------

  private logger = new Logger("AuthService");

  // ------------------------------

  constructor(private configService: ConfigService) {}

  // ------------------------------

  async getConfigOfApp(): Promise<any> {
    // Promise<{ [key: string]: string }> |
    // return this.configService.get<{ [key: string]: string }>("forFront");
  }

  // ------------------------------

  async checkStatus(info: any) {
    try {
      // const url = this.configService.get<string>("CheckOnline");
      const url = "dfsdf";
      const options = {
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
          Authorization: info.authorization,
        },
      };
      const res = await axios.get(url, options);

      return { ok: res.status };
    } catch (error) {
      throw new BadRequestException(error.response.data);
    }
  }

  // ------------------------------

  async checkRefreshToken(
    refreshToken: RefreshTokenDto
  ): Promise<AuthResponseDto> {
    // const url = this.configService.get<string>("RefreshToken");
    const url = "hfdfsd";
    try {
      const res = await axios.post(url, refreshToken);
      return res.data;
    } catch (error) {
      throw new BadRequestException(error.response.status);
    }
  }

  // ------------------------------

  async signIn(
    authCredentialsDto: AuthCredentialsDto
  ): Promise<AuthResponseDto> {
    try {
      // const url = this.configService.get<string>("tokenUrl");
      const url = "hfdfsd";

      const res = await axios.post(url, authCredentialsDto);
      return res.data;
    } catch (error) {
      throw new BadRequestException(error.response.data);
    }
  }

  // ------------------------------

  async signInOTP(authPhoneDto: AuthPhoneDto): Promise<void> {
    try {
      // const url = this.configService.get<string>("OTPUrl");
      const url = "hfdfsd";

      const res = await axios.post(url, authPhoneDto);
      return res.data;
    } catch (error) {
      throw new BadRequestException(error.response.data);
    }
  }

  // ------------------------------
}
