export * from "./1-Home-section/HomeSectionProvider";
export * from "./2-About-section/AboutProvider";
export * from "./3-Specialization-section/SpecializationProvider";
export * from "./4-Projects-section/ProjectsProvider";
export * from "./5-Exprience-section/ExprienceProvider";
export * from "./6-Testimonials-section/TestimonialsProvider";
export * from "./7-Partners-section/PartnersProvider";
export * from "./8-News-section/NewsProvider";
export * from "./9-Contact-section/ContactProvider";
